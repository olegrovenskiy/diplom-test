FROM nginx:latest

RUN mkdir -p /data/images

COPY ./nginx.conf /etc/nginx/nginx.conf

# copy image file to nginx  123

COPY 1.jpg /data/images/

ENTRYPOINT ["nginx", "-g", "daemon off;"]
